# Xen-Orchestra-Community

This repo builds xen-orchestra from the sources and enables https.
Launch with `https://localhost/`.

## Using a different Port

To use a different port, launch the build script with `PORT=5555 ./build.sh`
